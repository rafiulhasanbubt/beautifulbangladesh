//
//  DataModel.swift
//  BeautyOfBangladesh
//
//  Created by rafiul hasan on 2/27/20.
//  Copyright © 2020 rafiul hasan. All rights reserved.
//

import Foundation
import UIKit

struct DhakaModel {
    var title: String?
    var city: String?
    var placeImage: UIImage?
}

struct RajshahiModel {
    var title: String?
    var city: String?
    var placeImage: UIImage?
}

struct KhulnaModel {
    var title: String?
    var city: String?
    var placeImage: UIImage?
}

struct ChattogramModel {
    var title: String?
    var city: String?
    var placeImage: UIImage?
}

struct SylhetModel {
    var title: String?
    var city: String?
    var placeImage: UIImage?
}
