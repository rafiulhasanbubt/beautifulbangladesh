//
//  HomeViewController.swift
//  BeautyOfBangladesh
//
//  Created by rafiul hasan on 2/27/20.
//  Copyright © 2020 rafiul hasan. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var segmentView: UISegmentedControl!
    
    var dhData:[DhakaModel] = []
    var rsData:[RajshahiModel] = []
    var khData:[KhulnaModel] = []
    var cgData:[ChattogramModel] = []
    var stData:[SylhetModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        homeTableView.dataSource = self
        homeTableView.delegate = self
        
        let homeCellNib = UINib(nibName: "HomeTableViewCell", bundle: nil)
        homeTableView.register(homeCellNib, forCellReuseIdentifier: "HomeTableViewCell")
        
        dhData = [
            DhakaModel(title: "Ahsan Monjil", city:"City : Dhaka",placeImage:  #imageLiteral(resourceName: "Ahsan Manjil")),
            DhakaModel(title: "Shaheed Minar", city:"City : Dhaka",placeImage:  #imageLiteral(resourceName: "Shaheed Minar")),
            DhakaModel(title: "Lalbagh", city:"City : Dhaka",placeImage:  #imageLiteral(resourceName: "Lalbagh")),
            DhakaModel(title: "Sriti shoud", city:"City : Dhaka",placeImage:  #imageLiteral(resourceName: "Sriti shoud")),
            DhakaModel(title: "Sangshad", city:"City : Dhaka",placeImage:  #imageLiteral(resourceName: "Sangshad")),
            DhakaModel(title: "Dhakeshwari Temple", city:"City : Dhaka",placeImage:  #imageLiteral(resourceName: "Dhakeshwari Temple")),
            DhakaModel(title: "Bangladesh National Zoo", city:"City : Dhaka",placeImage:  #imageLiteral(resourceName: "National Zoo")),
            DhakaModel(title: "Star Mosque", city:"City : Dhaka",placeImage:  #imageLiteral(resourceName: "Star Mosque"))        
        ]

    }
    
    @IBAction func segmentPressed(_ sender: UISegmentedControl) {
        self.homeTableView.reloadData()
    }
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var value = 0
        switch segmentView.selectedSegmentIndex{
        case 0:
            value = dhData.count
            break
        case 1:
            value = rsData.count
            break
        case 2:
            value = khData.count
            break
        case 3:
            value = cgData.count
            break
        case 4:
        value = stData.count
        break
        default:
            break
        }
        return value
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        switch segmentView.selectedSegmentIndex{
        case 0:
            cell.dhModel = dhData[indexPath.row]
            break
        case 1:
            cell.rsModel = rsData[indexPath.row]
            break
        case 2:
            cell.khModel = khData[indexPath.row]
            break
        case 3:
            cell.cgModel = cgData[indexPath.row]
            break
        case 4:
            cell.stModel = stData[indexPath.row]
            break
        default:
            break
        }
        return cell
        
    }
    
}
