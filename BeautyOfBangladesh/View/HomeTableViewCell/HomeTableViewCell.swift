//
//  HomeTableViewCell.swift
//  BeautyOfBangladesh
//
//  Created by rafiul hasan on 2/27/20.
//  Copyright © 2020 rafiul hasan. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var placeImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        placeImage.layer.cornerRadius = 6.00
        placeImage.layer.borderWidth = 0.5
        placeImage.layer.borderColor = UIColor.lightGray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var dhModel:DhakaModel!{
        didSet{
            titleLabel.text = dhModel.title
            cityLabel.text = dhModel.city
            placeImage.image = dhModel.placeImage
        }
    }
    
    var rsModel:RajshahiModel!{
        didSet{
            titleLabel.text = rsModel.title
            cityLabel.text = rsModel.city
            placeImage.image = rsModel.placeImage
        }
    }

    var khModel:KhulnaModel!{
        didSet{
            titleLabel.text = khModel.title
            cityLabel.text = khModel.city
            placeImage.image = khModel.placeImage
        }
    }
    
    var cgModel:ChattogramModel!{
        didSet{
            titleLabel.text = cgModel.title
            cityLabel.text = cgModel.city
            placeImage.image = cgModel.placeImage
        }
    }
    
    var stModel:SylhetModel!{
        didSet{
            titleLabel.text = stModel.title
            cityLabel.text = stModel.city
            placeImage.image = stModel.placeImage
        }
    }
}
